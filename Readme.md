BouncyBlock
===================

BouncyBlock is a small Java tool, which blocks users on the forum, if the welcome email sent to them could not be delivered. 

###Usage
This project is a maven project. To build it, just run mvn clean install. 

To run the final jar, execute "java -jar BouncyBlock.jar < pathToLogfile> [pathToBlockedList]".

The first parameter is mandatory and should contain the path to the logfile of the mail server. The second parameter is optional and should contain a path to the list of blocked users. The default would be the current directory.

###How does it work?
The tool works, by going through the log files of the mailserver looking for bounce-entries, extracting the email address from these entries and calling another servlet to retrieve the user id associated with the email address. This user id is then blocked via the same servlet. All blocked users are saved into a file with their user id and email address. There will be 2 files created in the running directory:

- blacklist.lst
- bouncyblock.log

blacklist.lst contains the blocked email addresses and user ids, while bouncyblock.log is a logfile for the application, where every banned user is logged with a timestamp.

This tool could be scheduled via a cronjob and will then only block those users, who are not already in blacklist.lst.

