package me.randomdev.bouncyblock.impl;

import java.util.HashMap;
import java.util.HashSet;

import me.randomdev.bouncyblock.HttpService;

/** @author Steffen */
public class HttpServiceMockImpl implements HttpService {

	private HashMap<String, String> mockedData;

	private HashSet<String> disabledUsers, requestedEmails;

	public HttpServiceMockImpl() {
		disabledUsers = new HashSet<>();
		requestedEmails = new HashSet<>();
		mockedData = new HashMap<>();
	}

	public void setMockedData(HashMap<String, String> mockedData) {
		this.mockedData = mockedData;
	}

	public HashSet<String> getDisabledUsers() {
		return disabledUsers;
	}

	public HashSet<String> getRequestedEmails() {
		return requestedEmails;
	}

	@Override
	public String requestUserIdsSync(String email) {
		requestedEmails.add(email);
		if (mockedData.containsKey(email)) {
			return mockedData.get(email);
		}
		return "{\"data\":[]}";
	}

	@Override
	public String disableUser(String user) {
		disabledUsers.add(user);
		return "\"data\":\"OK\"";
	}
}
