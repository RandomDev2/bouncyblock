package me.randomdev.bouncyblock.loganalysis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.Test;

/** @author Steffen Jacobs */
public class TestBounceLogEntryStringSerializer {

	/** create, save, load and compare example data */
	@Test
	public void integrationTest() {
		BounceLogEntryDTO dto = new BounceLogEntryDTO("receiver", "dsn", "status");
		String data = BounceLogEntryStringSerializer.getInstance().serializeToString(dto);

		BounceLogEntryDTO deserializedDTO = BounceLogEntryStringSerializer.getInstance().deserializeFromString(data);

		assertEquals(dto, deserializedDTO);
	}

	@Test
	public void testSingleton() {
		assertSame(BounceLogEntryStringSerializer.getInstance(), BounceLogEntryStringSerializer.getInstance());
	}

	/** tests the BounceLogEntryStringSerializer with an illegal argument */
	@Test(expected = IllegalArgumentException.class)
	public void testFail() {
		String data = "receiver, dsn, status, error";
		BounceLogEntryStringSerializer.getInstance().deserializeFromString(data);
	}
}
