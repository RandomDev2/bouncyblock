package me.randomdev.bouncyblock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/** @author Steffen */
public class TestIOService {

	private static Set<String> testSet;
	private static String fileName;

	@BeforeClass
	public static void createTestSet() {
		testSet = new HashSet<String>();
		testSet.add("testString1");
		testSet.add("testString2");
		testSet.add(System.currentTimeMillis() + "");
		testSet.add("\u00c4\u00f6\u00df");
	}

	@BeforeClass
	public static void generateFileName() {
		fileName = "./blacklist_test_" + System.currentTimeMillis() + ".lst";
	}

	@Test
	public void testWriteRead() {
		IOService serv = IOService.getInstance();
		serv.writeSet(testSet, new File(fileName));

		Set<String> read = serv.readSet(new File(fileName));

		assertEquals(testSet.size(), read.size());
		read.forEach(s -> assertTrue(testSet.contains(s)));
	}
	
	@Test
	public void testSingleton(){
		assertSame(IOService.getInstance(), IOService.getInstance());
	}

	@AfterClass
	public static void removeTestFile() {
		new File(fileName).delete();
	}
}
