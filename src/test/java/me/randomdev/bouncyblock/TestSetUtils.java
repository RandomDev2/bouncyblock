package me.randomdev.bouncyblock;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

/** @author Steffen */
public class TestSetUtils {

	private static Set<String> set1, set2;

	@BeforeClass
	public static void generateSets() {
		set1 = new HashSet<>();
		set1.add("testString1");
		set1.add("testString2");
		set1.add(System.currentTimeMillis() + "");
		set1.add("\u00c4\u00f6\u00df");

		set2 = new HashSet<>();
		set2.add("testString2");
		set2.add((System.currentTimeMillis() - 500) + "");
		set2.add("\u00c4\u00f6\u00df");

		set1 = Collections.unmodifiableSet(set1);
		set2 = Collections.unmodifiableSet(set2);
	}

	@Test
	public void testIntersect() {
		Set<String> intersect = SetUtils.intersect(set1, set2);

		// Vx c intersect -> x c set1 ^ x c set2
		intersect.forEach(s -> assertTrue(set1.contains(s) && set2.contains(s)));

		// Vx c set1: x c set2 -> x c intersect
		set1.forEach(s -> {
			if (set2.contains(s)) {
				assertTrue(intersect.contains(s));
			}
		});

		// Vx c set2: x c set1 -> x c intersect
		set2.forEach(s -> {
			if (set1.contains(s)) {
				assertTrue(intersect.contains(s));
			}
		});
	}

	@Test
	public void testSubstract() {
		Set<String> subset = SetUtils.subtract(set1, set2);

		subset.forEach(e -> {
			// minimal
			if (set2.contains(e)) {
				fail("This element was not supposed to be in here.");
			}
			// complete
			if (!set1.contains(e)) {
				fail("Missing element.");
			}
		});
	}
}
