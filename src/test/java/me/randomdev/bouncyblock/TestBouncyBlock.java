package me.randomdev.bouncyblock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import me.randomdev.bouncyblock.impl.HttpServiceMockImpl;
import me.randomdev.bouncyblock.loganalysis.BounceLogEntryDTO;

/** @author Steffen */
public class TestBouncyBlock {

	private static String blacklistFile, logFile;
	private static Set<String> emailsInLogSet;
	private static Set<BounceLogEntryDTO> alreadyBannedSet;
	private static HashMap<String, String> emailsInLogJSONSet;
	private static HashMap<String, Integer> emailsInLogIdsSet;

	@BeforeClass
	public static void generateFileNames() {
		blacklistFile = "./blacklist_test_" + System.currentTimeMillis() + ".lst";
		logFile = "./logfile_test_" + System.currentTimeMillis() + ".log";
	}

	@BeforeClass
	public static void createTestSets() {
		emailsInLogSet = new HashSet<>();
		emailsInLogSet.add("test@example.com");
		emailsInLogSet.add("example@example.com");
		emailsInLogSet.add("thomas@example.com");
		emailsInLogSet.add("steffen@example.com");
		emailsInLogSet.add("testuser@example.com");
		emailsInLogSet = Collections.unmodifiableSet(emailsInLogSet);

		alreadyBannedSet = new HashSet<>();
		alreadyBannedSet.add(new BounceLogEntryDTO("test@example.com", "5.1.1", "bounced (Host or domain name not found."));
		alreadyBannedSet.add(new BounceLogEntryDTO("user@example.com", "5.1.1", "bounced (Host or domain name not found."));
		alreadyBannedSet.add(new BounceLogEntryDTO("someone@example.com", "5.1.1", "bounced (Host or domain name not found."));
		alreadyBannedSet.add(new BounceLogEntryDTO("example@example.com", "5.1.1", "bounced (Host or domain name not found."));
		alreadyBannedSet = Collections.unmodifiableSet(alreadyBannedSet);

		emailsInLogJSONSet = new HashMap<>();
		emailsInLogIdsSet = new HashMap<>();

		emailsInLogSet.forEach(e -> {
			emailsInLogJSONSet.put(e, String.format("{\"data\": [{\"id\": \"%d\"}]}", e.hashCode()));
			emailsInLogIdsSet.put(e, e.hashCode());
		});

		IOService.getInstance().writeDTOSet(alreadyBannedSet, new File(blacklistFile));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadInitialization() {
		new BouncyBlock().run();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadInitialization2() {
		new BouncyBlock().blacklistFile("").run();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadInitialization3() {
		new BouncyBlock().blacklistFile("").run();
	}

	@Test
	public void integrationTest() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		final String logLine = "Feb 25 14:31:42 ubuntu-VirtualBox postfix/smtp[3960]: 6D6C541FF2: to=<%s>, relay=mx01.emig.gmx.net[212.227.17.5]:25, delay=354, delays=354/0.01/0.18/0, dsn=%s, status=%s";

		Set<String> fakeLog = new HashSet<>();
		Set<BounceLogEntryDTO> dtoSet = new HashSet<>();

		emailsInLogSet.forEach(s -> {
			BounceLogEntryDTO dto = new BounceLogEntryDTO(s, "5.4.4", "bounced (Host or domain name not found.");
			fakeLog.add(String.format(logLine, dto.getEmailReceiver(), dto.getDsn(), dto.getStatus()));
			dtoSet.add(dto);
		});

		IOService.getInstance().writeSet(fakeLog, new File(logFile));

		BouncyBlock bb = new BouncyBlock().blacklistFile(blacklistFile).logFile(logFile);

		HttpServiceMockImpl httpMock = new HttpServiceMockImpl();
		httpMock.setMockedData(emailsInLogJSONSet);

		Field field = bb.getClass().getDeclaredField("httpService");
		field.setAccessible(true);
		field.set(bb, httpMock);

		assertSame(httpMock, field.get(bb));

		bb.run();

		Set<BounceLogEntryDTO> finalSet = IOService.getInstance().readDTOSet(new File(blacklistFile));

		// u: union
		// n: intersection
		// \: subtraction
		// <=>: equivalent
		// Vx: for all x (universal quantifier)
		// c: subset or element of
		// ^: logical and

		// check result
		// finalSet <=> originalSet u testSet

		// completeness
		assertTrue(finalSet.containsAll(alreadyBannedSet));
		assertTrue(finalSet.containsAll(dtoSet));

		// minimal
		finalSet.forEach(s -> assertTrue(alreadyBannedSet.contains(s) || dtoSet.contains(s)));

		// newly added emails: EmailsInLog \ (AlreadyBanned n EmailsInLog)
		Set<String> newEmails = SetUtils.subtract(emailsInLogSet,
				SetUtils.intersect(alreadyBannedSet.stream().map(BounceLogEntryDTO::getEmailReceiver).collect(Collectors.toSet()), emailsInLogSet));

		// check that every userID from a new email address is requested
		// |R|=|I| ^ Vx c I : x c R <--> equal sets
		assertEquals(newEmails.size(), httpMock.getRequestedEmails().size());
		httpMock.getRequestedEmails().forEach(e -> assertTrue(newEmails.contains(e)));

		// check that every new userID is blocked
		Set<String> expectedDisabledUserIds = new HashSet<String>();
		newEmails.forEach(email -> expectedDisabledUserIds.add("" + emailsInLogIdsSet.get(email)));

		assertEquals(newEmails.size(), expectedDisabledUserIds.size());

		// completeness and minimal
		httpMock.getDisabledUsers().forEach(u -> assertTrue(expectedDisabledUserIds.contains(u)));
		expectedDisabledUserIds.forEach(u -> assertTrue(httpMock.getDisabledUsers().contains(u)));
	}

	@AfterClass
	public static void removeTestFiles() {
		new File(blacklistFile).delete();
		new File(logFile).delete();
	}
}
