package me.randomdev.bouncyblock;

/** @author Steffen */
public class EntryPoint {
	public static void main(String... args) {
		if (args.length < 1) {
			System.out.println("Usage: java -jar BouncyBlock.jar <pathToLogfile> [pathToBlockedList]");
			return;
		}

		String logFile = args[0];
		String listFile = "./blacklist.lst";

		if (args.length >= 2) {
			listFile = args[1];
		}

		new BouncyBlock().blacklistFile(listFile).logFile(logFile).run();
	}
}
