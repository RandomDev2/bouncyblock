package me.randomdev.bouncyblock.loganalysis;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/** @author Steffen */
public class Deduplicator<T> {

	private final Set<T> set = new HashSet<>();

	/** @return true, if <i>t</i> is new */
	public boolean isNew(T t) {
		return set.add(t);
	}

	/** add <i>collection</i> to known objects */
	public void addValues(Collection<T> collection) {
		set.addAll(collection);
	}
}
