package me.randomdev.bouncyblock.loganalysis;

/** @author Steffen */
public class BounceLogEntryDTO {
	private final String emailReceiver, dsn, status;

	public BounceLogEntryDTO(String emailReceiver, String dsn, String status) {
		super();
		this.emailReceiver = emailReceiver;
		this.dsn = dsn;
		this.status = status;
	}

	/**
	 * @return the emailReceiver
	 */
	public String getEmailReceiver() {
		return emailReceiver;
	}

	/**
	 * @return the dsn
	 */
	public String getDsn() {
		return dsn;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((emailReceiver == null) ? 0 : emailReceiver.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BounceLogEntryDTO other = (BounceLogEntryDTO) obj;
		if (emailReceiver == null) {
			if (other.emailReceiver != null)
				return false;
		} else if (!emailReceiver.equals(other.emailReceiver))
			return false;
		return true;
	}
}