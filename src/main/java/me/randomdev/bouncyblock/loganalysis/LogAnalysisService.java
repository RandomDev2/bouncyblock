package me.randomdev.bouncyblock.loganalysis;

import java.util.Optional;

/** @author Steffen Jacobs */
public interface LogAnalysisService {
	public Optional<BounceLogEntryDTO> parseBounce(String l);
}
