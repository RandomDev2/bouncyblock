package me.randomdev.bouncyblock.loganalysis.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.randomdev.bouncyblock.loganalysis.BounceLogEntryDTO;
import me.randomdev.bouncyblock.loganalysis.LogAnalysisService;

/** @author Steffen */
public class LogAnalysisServiceImpl implements LogAnalysisService {

	private static final Pattern PAT_EMAIL_RECEIVER = Pattern.compile("to=<(.*@.*\\..*)>");
	private static final Pattern PAT_DSN = Pattern.compile("dsn=(([0-9](\\.[0-9]{1,3}){2})|([0-9]{3}))");
	private static final Pattern PAT_STATUS = Pattern.compile("status=(.*),?");
	private static final List<String> bounceCodes = Collections.unmodifiableList(Arrays.asList("4*", "5*"));

	private static LogAnalysisServiceImpl instance;

	private LogAnalysisServiceImpl() {
		// Singleton
	}

	public static LogAnalysisServiceImpl getInstance() {
		if (instance == null) {
			instance = new LogAnalysisServiceImpl();
		}
		return instance;
	}

	/**
	 * @return a BounceLogEntryDTO, if <i>l</i> contains a bounce. <br/>
	 *         Optional.empty() else
	 */
	public Optional<BounceLogEntryDTO> parseBounce(String l) {
		Matcher dsnMatcher = PAT_DSN.matcher(l);
		if (!dsnMatcher.find()) {
			return Optional.empty();
		}

		String dsn = dsnMatcher.group(1);

		if (!isBannableBounce(dsn)) {
			return Optional.empty();
		}

		Matcher emailReceiverMatcher = PAT_EMAIL_RECEIVER.matcher(l);
		if (!emailReceiverMatcher.find()) {
			return Optional.empty();
		}

		String emailReceiver = emailReceiverMatcher.group(1);
		String status = "";

		Matcher statusMatcher = PAT_STATUS.matcher(l);
		if (statusMatcher.find()) {
			status = statusMatcher.group(1);
		}

		return Optional.of(new BounceLogEntryDTO(emailReceiver, dsn, status));
	}

	/**
	 * @return whether the DSN (aka. bounce status code) is a bounce code that
	 *         should be banned
	 */
	private boolean isBannableBounce(String dsn) {
		for (String code : bounceCodes) {
			if (code.endsWith("*")) {
				code = code.substring(1, code.length() - 1);
				if (dsn.startsWith(code)) {
					return true;
				}
			}
			if (dsn.equals(code)) {
				return true;
			}
		}

		return false;
	}
}
