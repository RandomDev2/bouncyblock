package me.randomdev.bouncyblock.loganalysis;

/** @author Steffen */
public class BounceLogEntryStringSerializer {

	private static final String SPLITTER = ", ";

	private static BounceLogEntryStringSerializer instance;

	private BounceLogEntryStringSerializer() {
		// Singleton
	}

	public static BounceLogEntryStringSerializer getInstance() {
		if (instance == null) {
			instance = new BounceLogEntryStringSerializer();
		}
		return instance;
	}

	/**serializes the DTO to a human readable format to save it later*/
	public String serializeToString(BounceLogEntryDTO dto) {
		StringBuffer buf = new StringBuffer();
		buf.append(dto.getEmailReceiver());
		buf.append(SPLITTER);
		buf.append(dto.getDsn());
		buf.append(SPLITTER);
		buf.append(dto.getStatus());
		return buf.toString();

	}

	/**deserializes the DTO from the human readable format*/
	public BounceLogEntryDTO deserializeFromString(String serialized) {
		String[] data = serialized.split(SPLITTER);
		if (data.length != 3) {
			throw new IllegalArgumentException("Input must be of structure 'EMAIL,DSN,STATUS'. Input was (" + serialized + ").");
		}

		return new BounceLogEntryDTO(data[0], data[1], data[2]);
	}
}
