package me.randomdev.bouncyblock;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import me.randomdev.bouncyblock.loganalysis.BounceLogEntryDTO;
import me.randomdev.bouncyblock.loganalysis.BounceLogEntryStringSerializer;

/** @author Steffen */
public class IOService {

	private static final Logger LOG = Logger.getLogger(IOService.class.getName());

	private static IOService instance;

	private IOService() {
		// Singleton
	}

	public static IOService getInstance() {
		if (instance == null) {
			instance = new IOService();
		}
		return instance;
	}

	public Set<String> readSet(File f) {
		final Set<String> set = new HashSet<String>();
		try {
			Files.newBufferedReader(f.toPath(), Charset.forName("UTF-8")).lines().forEach(l -> set.add(l));
		} catch (IOException e) {
			LOG.severe(e.getMessage());
		}

		return set;
	}

	public void writeSet(Set<String> set, File f) {
		
		try (BufferedWriter bw = Files.newBufferedWriter(f.toPath(), Charset.forName("UTF-8"))) {
			for (String s : set) {
				bw.write(s);
				bw.write("\n");
			}
		} catch (IOException ioe) {
			LOG.severe(ioe.getMessage());
		}
	}
	
	public void writeDTOSet(Set<BounceLogEntryDTO> set, File f){
		try (BufferedWriter bw = Files.newBufferedWriter(f.toPath(), Charset.forName("UTF-8"))) {
			for (BounceLogEntryDTO dto : set) {
				bw.write(BounceLogEntryStringSerializer.getInstance().serializeToString(dto));
				bw.write("\n");
			}
		} catch (IOException ioe) {
			LOG.severe(ioe.getMessage());
		}
	}
	
	public Set<BounceLogEntryDTO> readDTOSet(File f) {
		final Set<BounceLogEntryDTO> set = new HashSet<BounceLogEntryDTO>();
		try {
			Files.newBufferedReader(f.toPath(), Charset.forName("UTF-8")).lines().forEach(l -> set.add(BounceLogEntryStringSerializer.getInstance().deserializeFromString(l)));
		} catch (IOException e) {
			LOG.severe(e.getMessage());
		}

		return set;
	}
}
