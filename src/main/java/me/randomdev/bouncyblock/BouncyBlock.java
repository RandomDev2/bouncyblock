package me.randomdev.bouncyblock;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Optional;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import me.randomdev.bouncyblock.impl.HttpServiceImpl;
import me.randomdev.bouncyblock.loganalysis.BounceLogEntryDTO;
import me.randomdev.bouncyblock.loganalysis.Deduplicator;
import me.randomdev.bouncyblock.loganalysis.LogAnalysisService;
import me.randomdev.bouncyblock.loganalysis.impl.LogAnalysisServiceImpl;

/** @author Steffen */
public class BouncyBlock implements Runnable {

	private static final Logger LOG = Logger.getLogger(BouncyBlock.class.getName());
	private static final String BLOCK_STRING = "Blocking user '%d' - '%s' for reason: '%s'";
	private static final String LOGFILE = "./bouncyblock.log";

	private String blacklistFile, logFile;

	private HttpService httpService = HttpServiceImpl.getInstance();
	private LogAnalysisService logAnalysisService = LogAnalysisServiceImpl.getInstance();
	private IOService ioService = IOService.getInstance();

	public BouncyBlock() {
		try {
			FileHandler handler = new FileHandler(LOGFILE);
			handler.setFormatter(new SimpleFormatter());
			LOG.addHandler(handler);
		} catch (SecurityException | IOException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	public BouncyBlock blacklistFile(String listFile) {
		this.blacklistFile = listFile;
		return this;
	}

	public BouncyBlock logFile(String logFile) {
		this.logFile = logFile;
		return this;
	}

	private void disableUser(String email, String reason) {
		String jsonResult = httpService.requestUserIdsSync(email);
		if (jsonResult == null) {
			LOG.severe("Bad HTTP result.");
			return;
		}

		JSONArray jarr = new JSONObject(jsonResult).getJSONArray("data");

		for (int i = 0; i < jarr.length(); i++) {
			int id = jarr.getJSONObject(i).getInt("id");
			httpService.disableUser("" + id);
			LOG.info(String.format(BLOCK_STRING, id, email, reason));
		}
	}

	@Override
	public void run() {
		if (this.blacklistFile == null) {
			throw new IllegalArgumentException("call bouncyBlock.listFile(String fileName) before.");
		}

		if (this.logFile == null) {
			throw new IllegalArgumentException("call bouncyBlock.logFile(String fileName) before.");
		}

		Set<BounceLogEntryDTO> blockedUsers = ioService.readDTOSet(new File(this.blacklistFile));

		Deduplicator<BounceLogEntryDTO> dedup = new Deduplicator<>();
		dedup.addValues(blockedUsers);
		try {

			Files.newBufferedReader(new File(logFile).toPath(), Charset.forName("UTF-8"))
			.lines()
			.map(l -> logAnalysisService.parseBounce(l))
			.filter(Optional::isPresent)
			.filter(o -> dedup.isNew(o.get()))
			.forEach(o -> {
						disableUser(o.get().getEmailReceiver(), o.get().getStatus());
						blockedUsers.add(o.get());
					});

		} catch (IOException e) {
			e.printStackTrace();
		}

		ioService.writeDTOSet(blockedUsers, new File(blacklistFile));
	}
}
