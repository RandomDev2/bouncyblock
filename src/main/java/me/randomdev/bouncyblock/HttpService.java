package me.randomdev.bouncyblock;

/** @author Steffen */
public interface HttpService {
	/**
	 * requests the user ID via HTTP synchronously
	 * 
	 * @param email
	 *            the email address of the user to request
	 * @returns the userID associated with the email
	 */
	public String requestUserIdsSync(String email);

	/**
	 * disables a user
	 * 
	 * @param user
	 *            the userID of the user to disable
	 */
	public String disableUser(String user);
}
