package me.randomdev.bouncyblock;

import java.util.HashSet;
import java.util.Set;

/** @author Steffen */
public class SetUtils {

	private SetUtils() {
		throw new AssertionError("Singleton");
	}

	/**
	 * intersect set1 and set2: return elements, which are contained in both
	 * sets
	 */
	public static <T> Set<T> intersect(Set<T> set1, Set<T> set2) {
		Set<T> result = new HashSet<T>();

		set1.forEach(t -> {
			if (set2.contains(t)) {
				result.add(t);
			}
		});

		return result;
	}

	/**
	 * subtract set2 from set1: return elements, which are in set1 but not in
	 * set2
	 */
	public static <T> Set<T> subtract(Set<T> set1, Set<T> set2) {
		Set<T> result = new HashSet<T>();

		set1.forEach(t -> {
			if (!set2.contains(t)) {
				result.add(t);
			}
		});

		return result;
	}
}
