package me.randomdev.bouncyblock.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.randomdev.bouncyblock.HttpService;

/** @author Steffen */
public class HttpServiceImpl implements HttpService {

	private static final String URL_DISABLE_USER = "http://example.com/srv/adm/endpoint.php/user/%s/disable";
	private static final String URL_REQUEST_NAME = "http://example.com/srv/adm/endpoint.php/users/?email=%s?state=all";
	private static final Logger LOG = Logger.getLogger(HttpServiceImpl.class.getName());

	private static HttpServiceImpl instance;

	public String requestUserIdsSync(String email) {
		return executeGetRequestSync(String.format(URL_REQUEST_NAME, email), 2000);
	}

	public String disableUser(String user) {
		return executePostRequestSync(String.format(URL_DISABLE_USER, user), 2000);
	}

	private String executeGetRequestSync(String url, int timeout) {

		HttpURLConnection c = null;
		try {
			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
				return sb.toString();
			default:
				LOG.warning(String.format("unrecognized HTTP status code %d for get-request %s", status, url));
				return null;
			}

		} catch (IOException ex) {
			LOG.log(Level.SEVERE, String.format("Error executing get-request %s: %s", url, ex.getMessage()), ex);
		} finally {
			if (c != null) {
				try {
					c.disconnect();
				} catch (Exception ex) {
					LOG.log(Level.WARNING, String.format("Error executing get-request %s: %s", url, ex.getMessage()), ex);
				}
			}
		}
		return null;
	}

	private String executePostRequestSync(String url, int timeout) {

		HttpURLConnection c = null;
		try {
			URL u = new URL(url);

			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("POST");
			// c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();

			c.setRequestMethod("POST");
			c.setDoOutput(true);
			c.setRequestProperty("Content-Type", "application/json");

			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
				return sb.toString();
			default:
				LOG.warning(String.format("unrecognized HTTP status code %d for get-request %s", status, url));
				return null;
			}

		} catch (IOException ex) {
			LOG.log(Level.SEVERE, String.format("Error executing post-request %s: %s", url, ex.getMessage()), ex);
		} finally {
			if (c != null) {
				try {
					c.disconnect();
				} catch (Exception ex) {
					LOG.log(Level.WARNING, String.format("Error executing post-request %s: %s", url, ex.getMessage()), ex);
				}
			}
		}
		return null;
	}

	private HttpServiceImpl() {
		// singleton
	}

	public static HttpServiceImpl getInstance() {
		if (instance == null) {
			instance = new HttpServiceImpl();
		}
		return instance;
	}
}
